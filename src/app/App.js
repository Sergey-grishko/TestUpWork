import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Main from './components/Main/Main';

class App extends Component {

    render() {
    return (
        <div className="App">
            <header>
                <Header />
            </header>

            <main>
                <Main />
            </main>
        </div>
    );
  }
}

export default App;
