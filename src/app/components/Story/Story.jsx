import React from 'react';
import './Story.css';
import MenuBar from './MenuBar';

function Story({index,
                   image,
                   cachedLogo,
                   title,
                   description,
                   score,
                   showDetails,
                   followers,
                   stories,
                   publish,
                   url}) {
    let allStories = stories;
    let time = new Date (Date.now() - new Date(publish)).getHours();
    return (
        <div className="story">
            <div className="story-content">
                <div className="story-logo">
                    <img src={image === null ? cachedLogo : image} alt=""/>
                </div>

                <div className="story-items">
                    <div className="story-text">
                        <h4 onClick={()=>{window.open(url, "_blank")}} >{title}</h4>
                        <div className="followers">
                            {followers ?
                                <div className="twitter-followers">
                                    <i className="fab fa-twitter"></i>
                                    <h4 className="hours">@CNBC {time} hours ago follower / ing ratio: {followers}</h4>
                                </div>
                                :
                                <h4 className="hours">CNBC {time} hours ago</h4>
                            }
                        </div>
                    </div>

                    <div className="score">
                        {score}%
                    </div>

                    {!allStories[index].showDetails ?
                        <div className="arrow" onClick={()=> {
                            allStories[index].showDetails = true;
                            showDetails(allStories)
                        }}>
                            <i className="fa fa-angle-down"></i>
                        </div>
                        :
                        <div className="arrow" onClick={()=> {
                            allStories[index].showDetails = false;
                            showDetails(allStories)
                        }}>
                            <i className="fa fa-angle-up"></i>
                        </div>
                    }


                </div>

            </div>

            {allStories[index].showDetails ?
                <div className="description">
                    <p>{description}</p>
                </div>
                : null
            }

            <div className="more-less">
                {!allStories[index].showDetails ?
                    <div className="more" onClick={()=> {
                        allStories[index].showDetails = true;
                        showDetails(allStories)}}>
                        <h4>More</h4>
                        <i className="fa fa-angle-down"></i>
                    </div>
                    :
                    <div className="less" onClick={()=> {
                        allStories[index].showDetails = false;
                        showDetails(allStories)}}>
                        <h4>Less</h4>
                        <i className="fa fa-angle-up"></i>
                    </div>
                }
            </div>

            {allStories[index].showDetails ?
                <div className="footer-content">
                    <MenuBar />
                </div>
                : null
            }

        </div>
    )
}

export default Story;