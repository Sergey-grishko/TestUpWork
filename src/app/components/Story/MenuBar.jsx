import React from 'react';

function MenuBar() {
    return (
        <div className="menu-bar">
            <div className="bookmark">
                <i className="far fa-bookmark"></i>
                <h6>Bookmark</h6>
            </div>

            <div className="like">
                <i className="far fa-thumbs-up fa-flip-horizontal"></i>
                <h6>Like</h6>
            </div>

            <div className="dislike">
                <i className="far fa-thumbs-down fa-flip-horizontal"></i>
                <h6>Dislike</h6>
            </div>
        </div>
    )
};

export default MenuBar;