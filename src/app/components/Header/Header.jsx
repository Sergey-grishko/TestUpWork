import React, { Component } from 'react';
import logo from '../../../assets/imgs/logo.png';
import userProfile from '../../../assets/imgs/user_photo.png'
import './Header.css';

export default class Header extends Component {

    constructor() {
        super();

        this.state = {
            showProfile: false
        }
    }

    setProfile(value) {
        this.setState({
            showProfile: value
        })
    };

    render() {
        return (
            <div className="header">
                <div className="menu-icon">
                    <i className="fas fa-bars"></i>
                </div>
                <div className="logo">
                    <img src={logo} alt=""/>
                </div>
                <div className={this.state.showProfile ? 'user-clicked' : 'user'} onClick={()=>{
                    this.setProfile(!this.state.showProfile)
                }}>
                    <img src={userProfile} alt=""/>
                    <div className="user-name">
                        <h4>Jane Doe</h4>
                        {this.state.showProfile ?
                            <i className="fas fa-sort-up up-icon"></i>
                            :
                            <i className="fas fa-sort-down down-icon"></i>
                        }
                    </div>
                </div>

                {this.state.showProfile ?
                    <div className="profile-content">
                        Sign Out
                    </div>
                    : null
                }
            </div>
        )
    }

}