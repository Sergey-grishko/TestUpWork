import React, {Component} from 'react';
import Story from '../Story/Story';
import stories from "../../../stories/data";

export default class LeftContent extends Component {

    constructor() {
        super();

        this.state = {
            stories: stories
        };
    }

    showDetails = (state) => {
        this.setState({
            stories: state
        })
    };

    render() {
        console.log(stories)
        return (

            <div className="left-content">
                <h1 className="content-header">Watchlist</h1>

                <div className="content">
                    {
                        this.state.stories.map((story, index) => {
                            return (
                                <Story
                                    stories={this.state.stories}
                                    key={index}
                                    index={index}
                                    image={story.author_image_url}
                                    cachedLogo={story.domain_cached_logo_url}
                                    title={story.title}
                                    description={story.description}
                                    score={story.score}
                                    followers={story.author_followers_count}
                                    showDetails={this.showDetails}
                                    publish={story.publishTime}
                                    url={story.url}/>
                            )

                        })
                    }

                </div>
            </div>
        )
    }
}