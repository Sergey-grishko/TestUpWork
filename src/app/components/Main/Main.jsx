import React, { Component } from 'react';
import './Main.css';
import LeftContent from './LeftContent';

export default class Main extends Component {

    constructor() {
        super();

        this.state = {
            showProfile: null
        }
    }

    render() {
        return (
            <div className="app">
                <div className="main">
                    <div className="left-side">
                        <LeftContent />
                    </div>

                    <div className="right-side"></div>

                </div>
            </div>
        )
    }
}